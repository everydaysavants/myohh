var webpack = require("webpack");

module.exports = {
    entry: {
        app: "./src/App.js"
    },
    output: {
        path: '../src/main/resources/static/',
        filename: "app.js"
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    module: {
        loaders: [
            {test: /\.less$/, exclude: /node_modules/, loader: "style!css!less" },
            {test: /\.(?:js|jsx)$/, exclude: /node_modules/, loaders: ['babel-loader']}
        ]
    }
};
