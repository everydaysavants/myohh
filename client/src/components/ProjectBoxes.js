import React from 'react';
import Header from './Header';
import Footer from './Footer';
import ProjectBox from './ProjectBox';
import axios from 'axios';

export default class ProjectBoxes extends React.Component {

    constructor(props){
        super(props);
        this.state = {projects:[]}
    }

    componentDidMount() {
        axios.get('/projects').then((projectResults) => {
            this.setState({
                projects: projectResults.data
            })
        }).catch((error) => {

            console.log("error occurred", error);
        });
    }

    render() {
        //If projects are empty this will break? Do we care?
        let projects = this.state.projects.map(project => {
            return (
                <ProjectBox key={project.id} project={project} />
            );
        });
        return (
            <div className="row">
                {projects}
            </div>
        );
    }
}

ProjectBoxes.propTypes = {hideLessons: React.PropTypes.bool};