import React from 'react';
import LessonBox from './LessonBox';
import axios from 'axios';

export default class LessonBoxes extends React.Component {

    constructor(props){
        super(props);
        console.log('this.props from constructor', props)
        this.state = {lessons:[]}
    }

    componentDidMount() {
        let { classes } = this.props;
        axios.get('/lessons').then((courseResults) => {
            if(!classes) {
                this.setState({
                    lessons: courseResults.data
                })
            }
            else {
                let projectLessons = courseResults.data.filter(data => classes.includes(data.id));
                this.setState({
                    lessons: projectLessons
                })
            }
        }).catch((e) => {
            console.log("error occurred", e);
        });
    }

    render() {

        let lessons = this.state.lessons.map(lesson => {
            return (
                <LessonBox key={lesson.id} title={lesson.title}
                           summary={lesson.summary}/>
            );
        });

        return (
            <div className="row">
                <div className="col-sm-12">
                    <div className="row">
                        {lessons}
                    </div>
                </div>
            </div>
        );
    }
}

LessonBoxes.propTypes = {classes: React.PropTypes.array};