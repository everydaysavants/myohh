import React from 'react';
import {Link, PropTypes} from 'react-router';
import AuthStatus from './AuthStatus';
import {Nav, Navbar, NavItem, Carousel} from 'react-bootstrap';


class Header extends React.Component {

    constructor(props) {
        super(props);
        this.navigateHome = this.navigateHome.bind(this);
        this.navigateCourses = this.navigateCourses.bind(this);
        this.navigateAboutUs = this.navigateAboutUs.bind(this);
        this.navigateBlog = this.navigateBlog.bind(this);
        this.navigatePayment = this.navigatePayment.bind(this);
    }

    navigateCourses(){
        this.context.router.push("/app/lessons");
    }

    navigateHome(){
        this.context.router.push("/");
    }

    navigateAboutUs(){
        this.context.router.push("/app/about-us");
    }

    navigateBlog(){
        this.context.router.push("/app/blog");
    }

    navigatePayment(){
        this.context.router.push("/app/payment");
    }

    render() {
        return (
            <div>
                <Navbar>
                    <Navbar.Header>
                        <a href="#" className="pull-left"><img src="/images/myorangehardhatlogo_notext_hres.png" height="40" width="40"/></a>
                        <Navbar.Brand>
                            <Link to="/">My Orange Hard Hat</Link>
                        </Navbar.Brand>
                        <Navbar.Toggle />
                    </Navbar.Header>

                    <Navbar.Collapse>
                            <Nav>
                                <NavItem onClick={this.navigateHome}>Home</NavItem>
                                <NavItem onClick={this.navigateCourses}>Classes</NavItem>
                                <NavItem onClick={this.navigateAboutUs}>About Us</NavItem>
                             </Nav>
                            <Nav pullRight>
                                <NavItem onClick={this.navigateBlog}>Blog</NavItem>
                                <NavItem onClick={this.navigatePayment}>Payment</NavItem>
                            </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </div>
        )
    }
}

Header.contextTypes = {
    router: function() { return React.PropTypes.func.isRequired; }
};

export default Header;

