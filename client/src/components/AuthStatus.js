import React from 'react';
import {Link} from 'react-router';
import axios from 'axios';

export default class AuthStatus extends React.Component {
    componentWillMount() {
        this.setState({
            loggedIn: false
        })
    }

    componentDidMount() {
        axios.get('/authStatus').then((authResponse) => this.setState({
            loggedIn: authResponse.data
        })).catch(() => {
            this.setState({
                loggedIn: false
            })
        });
    }
    render() {
        return (
            <ul className="nav navbar-nav navbar-right">
                <li className={this.state.loggedIn ? 'hide' : 'show'}><Link to="/login">Login</Link></li>
                <li className={this.state.loggedIn ? 'show' : 'hide'}><a href="/logout">Logout</a></li>
            </ul>
        );
    }
}