import React from 'react';
import { Panel } from 'react-bootstrap';
import { Link } from 'react-router';

export default class ProjectBox extends React.Component {

    constructor(props) {
        super(props);
    }

    onCLickDetailLink(e) {
        e.preventDefault();
        this.context.router.push("/app/project/" + this.props.project.id);
    };

    onClickRegister(e) {
        e.preventDefault();
        this.context.router.push("/app/register");
    }

    render() {
        const backGroundHeader = {
            backgroundColor: "darkorange",
            color: "white",
            fontSize: "large"
        };

        const backGroundBody = {
            backgroundColor: "#f2f2f2"
        };

        const buttonStyleDetail = {
            backgroundColor:"darkorange",
            float: "left",
            color:"white"
        };

        const buttonStyleRegister = {
            float: "right"
        };

        const linkStyleDetails = {
            color: "blue"
        }

        return (
            <div className="col-md-4">
                <div className="panel">
                    <div className="panel-heading" style={backGroundHeader}>
                        {this.props.project.title}
                    </div>
                    <div className="panel-body" style={backGroundBody}>
                        {this.props.project.description}
                        <br/>
                        <br/>
                        <Link style={linkStyleDetails} to="/app/details" onClick={this.onCLickDetailLink.bind(this)}>Details>></Link>
                        <button className="btn btn-success" style={buttonStyleRegister} onClick={this.onClickRegister.bind(this)}>Register</button>
                    </div>
                </div>
            </div>
        )
    }
}

ProjectBox.propTypes = {project: React.PropTypes.object};

ProjectBox.contextTypes = {
    router: function() { return React.PropTypes.func.isRequired; }
};