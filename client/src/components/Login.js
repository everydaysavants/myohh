import React from 'react';
import Header from './Header'
import Footer from './Footer'
import axios from 'axios'

export default class Login extends React.Component {
    componentWillMount() {
        this.setState({
            errorMessage: '',
            email: '',
            password: ''
        });
    }
    changeHandler(event) {
        if(event.target.name === 'email') {
            this.setState({
                errorMessage: this.state.errorMessage,
                email: event.target.value,
                password: this.state.password
            });
        } else {
            this.setState({
                errorMessage: this.state.errorMessage,
                email: this.state.email,
                password: event.target.value
            });
        }
    }
    submitHandler() {
        let email = this.state.email;
        let password = this.state.password;
        axios.post("/login", {email: email, password: password}).then((response) => {

        }).catch((error) => {
           this.setState({
               errorMessage: "An error occurred during login"
           })
        });
    }
    render() {
        return (
            <div>
            <Header selectedPage="home" loggedIn={this.props.loggedIn}></Header>
                <div className="container">
                    <div className="masthead">
                        <img src="/images/myorangehardhatlogo.png" />
                    </div>
                    <div>{this.state.errorMessage}</div>
                    <div className="email-label">
                        <label>E-mail</label>
                    </div>
                    <div className="email">
                        <input className="input-box" name="email" maxLength="75"
                               type="text" placeholder="yourname@email.com"
                               value={this.state.email}
                               onChange={this.changeHandler.bind(this)}/>
                    </div>
                    <div className="password-label">
                        <label>Password</label>
                    </div>
                    <div className="">
                        <input className="password" name="password"
                               maxLength="50" type="password"
                               placeholder="Password"
                               value={this.state.password}
                               onChange={this.changeHandler.bind(this)}/>
                    </div>
                    <div className="remember-me-label">
                        <input type="checkbox" name="remember"/>
                        <label>Remember Me</label>
                        <input className="sign-in-button" type="button"
                               value="Sign In" onClick={this.submitHandler.bind(this)}/>
                    </div>
                    <div>
                        <a className="forget-password" href="#/forgot-password">Forgot Your Password?</a>
                        <a className="create-account" href="#/sign-up">Create an Account</a>
                    </div>
                    <Footer />
                </div>
            </div>
        )
    }
}