import React from 'react';
import {Panel} from 'react-bootstrap'

export default class LessonBox extends React.Component {
    render() {

        const backGroundHeader = {
            backgroundColor: "darkgrey",
            color: "white",
            fontSize: "large"
        };

        const backGroundBody = {
            backgroundColor: "#f2f2f2"
        };

        return (
            <div className="col-md-4">
                <div className="panel">
                    <div className="panel-heading" style={backGroundHeader}>
                        {this.props.title}
                    </div>
                    <div className="panel-body" style={backGroundBody}>
                        {this.props.summary}
                    </div>
                </div>
            </div>
        )

    }
}