import React from 'react';

export default class TextRotator extends React.Component {
    componentWillMount(){
        this.setState({
            displayItems: [],
            index: 0
        })
    }
    componentDidMount() {
        let displayItems = this.props.displayItems.split(",");
        this.setState({
            displayItems: displayItems,
            index: 0
        });
        this.interval = this.setTimer();
    }
    render() {
        return (
                <span className="orange rotate-span">
                     {this.state.displayItems[this.state.index]}
                </span>
        )
    }
    componentWillUnmount(){
        clearInterval(this.interval);
    }
    setTimer(){
        return (setInterval(() => {
            let index = this.state.index;
            let length = this.state.displayItems.length;

            this.setState({
                index: (index + 1) % length
            });
        }, 1500));
    }
}