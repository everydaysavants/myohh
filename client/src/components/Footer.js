import React from 'react';

export default class Footer extends React.Component {
    render() {
        return (
            <div className="navbar navbar-fixed-bottom footerMohh">
                 &copy; 2016 My Orange Hard Hat Inc. All rights reserved.
            </div>
        )
    }
}