
import React from 'react';

export default class UserGridRow extends React.Component {
    render() {
        return (
            <tr>
                <th>
                    {this.props.name}
                </th>
                <th>
                    {this.props.age}
                </th>
                <th>
                    {this.props.email}
                </th>
                <th>
                    {this.props.courseName}
                </th>
                <th>
                    {this.props.startDate}
                </th>
            </tr>
        )
    }
}
