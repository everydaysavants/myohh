import React from 'react';
import ReactDOM from 'react-dom';
import Header from './../components/Header';
import Footer from './../components/Footer';
import axios from 'axios';
import {Form, FormGroup, Col, ControlLabel, FormControl, Checkbox, Button, ListGroup, ListGroupItem} from 'react-bootstrap';

export default class Register extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            //Maybe extract out constants for these values and use it in difft places
            formData: {
                firstName: '',
                lastName: '',
                age: 0,
                email: '',
                courseName: '',
                startDate: ''
            },
            validated: true,
            validationMessage: ''
        }
    }

    componentDidMount(){
        if(!this.state.validated)
            ReactDOM.findDOMNode(this.refs.validation).focus();
    }

    handleClick(e) {
        e.preventDefault();
        //console.log('this.state.formData' ,this.state.formData);
        axios.post('/signup', this.state.formData).then((result) => {
            const navigateTo = "/app/confirm/" + result.data;
            this.context.router.push(navigateTo);
        }).catch((err) => {
            if(err.status == 400) {
                this.setState({validated: false, validationMessage: err.data.message});
            }
        });
    }

    onChangeHandler(e){
        this.state.formData[e.target.id] = e.target.value;
    }

    render() {
        return (
            <div>
                <Header selectedPage="home"></Header>
                <div className="container paddingTop20">
                    <form>
                        { !this.state.validated ? <formGroup ref="validation" controlId="validation">
                            <ListGroup>
                                <ListGroupItem bsStyle="danger">
                                    {this.state.validationMessage}
                                </ListGroupItem>
                            </ListGroup>
                        </formGroup> : null }
                        <FormGroup controlId="firstName">
                            <ControlLabel>First Name</ControlLabel>
                            <FormControl type="text" placeholder="First Name"
                                         onChange={this.onChangeHandler.bind(this)}/>
                        </FormGroup>
                        <FormGroup controlId="lastName">
                            <ControlLabel>Last Name</ControlLabel>
                            <FormControl type="text" placeholder="Last Name"
                                         onChange={this.onChangeHandler.bind(this)}/>
                        </FormGroup>
                        <FormGroup controlId="age">
                            <ControlLabel>Age</ControlLabel>
                            <FormControl type="number" placeholder="Age"
                                         onChange={this.onChangeHandler.bind(this)}/>
                        </FormGroup>
                        <FormGroup controlId="email">
                            <ControlLabel>Email address</ControlLabel>
                            <FormControl type="email"
                                         placeholder="email"
                                         onChange={this.onChangeHandler.bind(this)}/>
                        </FormGroup>
                        <FormGroup controlId="courseName">
                            <ControlLabel>Select Course</ControlLabel>
                            <FormControl componentClass="select" placeholder="select class"
                                         onChange={this.onChangeHandler.bind(this)}>
                                <option value="select">select</option>
                                <option value="PROJ1">Learn to Code: Hack Arduino (8+)</option>
                                <option value="PROJ3">Young Hackers: Hack Arduino (6+)</option>
                                <option value="PROJ2">Coding in Scratch</option>
                                <option value="PROJ4">Learn Electronics using SnapCircuits</option>
                            </FormControl>
                        </FormGroup>
                        <FormGroup controlId="startDate"
                                   onChange={this.onChangeHandler.bind(this)}>
                            <ControlLabel>Start Date</ControlLabel>
                            <FormControl componentClass="select" placeholder="select date">
                                <option value="select">select</option>
                                <option value="June 10">Friday June 10 (5:30 - 6:30)</option>
                                <option value="June 12">Sunday June 12 (4:00 - 5:00)</option>
                                <option value="June 13">Monday June 13 (5:30 - 6:30)</option>
                                <option value="June 17">Friday June 17 (5:30 - 6:30)</option>
                                <option value="June 19">Sunday June 19 (4:00 - 5:00)</option>
                                <option value="June 20">Monday June 20 (5:30 - 6:30)</option>
                                <option value="June 24">Friday June 24 (5:30 - 6:30)</option>
                                <option value="June 26">Sunday June 26 (4:00 - 5:00)</option>
                                <option value="June 27">Monday June 27 (5:30 - 6:30)</option>
                            </FormControl>
                        </FormGroup>
                        <Button type="submit" onClick={this.handleClick.bind(this)}>
                            Submit
                        </Button>
                    </form>
                </div>
                <Footer />
            </div>
        );
    }
}

Register.contextTypes = {
    router: function() { return React.PropTypes.func.isRequired; }
};
