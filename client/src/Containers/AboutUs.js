import React from 'react';
import Header from './../components/Header';
import Footer from './../components/Footer';
import { Panel } from 'react-bootstrap'

export default class AboutUs extends React.Component {
    render() {
        const imageStyle = {
            paddingBottom: '20px',
            maxWidth: '100%',
            maxHeight: '100%'
        };

        const paddingBottom = {
            paddingBottom: '20px'
        };

        return (
            <div>
                <Header selectedPage="about-us"></Header>
                <div className="container">
                    <img style={imageStyle} src="/images/carousel_1.jpg"/>
                    <h1>
                        About Us
                    </h1>
                    <div style={paddingBottom}>
                        We are a team of professionals working towards a common goal of teaching our kids new tools and
                        technology.
                        We are parents, geeks, teachers and lifetime learners ourselves. We are constantly playing with
                        new tools,
                        and working on modifying those tools to make them easy enough for a five year old to use.
                    </div>
                    <Panel header="Email us">
                        <a href="mailto:mail@myorangehardhat.com">mail@myorangehardhat.com</a>
                        <br/>
                        We are looking for feedback on type of course you would like to teach your child.
                    </Panel>
                    <Panel header="Call us">
                        513.201.7492
                    </Panel>

                    <Panel header="Class Location">8262 Highland Pointe Drive, <br/> West Chester, Ohio 45069</Panel>

                    <p></p>
                </div>
                <Footer />
            </div>
        )
    }
}