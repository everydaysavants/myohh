import React from 'react';
import Header from './../components/Header'
import Footer from './../components/Footer'

export default class Payment extends React.Component {
    render() {
        const imageStyle = {
            paddingBottom:'20px',
            maxWidth: '100%',
            maxHeight:'100%'
        };

        return (
            <div>
                <Header selectedPage="contact-us"></Header>
                <div className="container">
                    <img style={imageStyle} src="/images/carousel_3.jpg"/>
                    <h1>
                        Make Payment:
                    </h1>
                    <p>
                        Thank you for enrolling into the class. You will be redirected to the secure payment page after you click on Pay Now button below.
                    </p>
                    <p><span className="lead"><form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx">
                        <input type = "hidden" name = "LinkId" value ="1fb1048c-f62a-463b-bcc8-6588840f79ba" />
                        <input type = "submit" value = "Pay Now" />
                    </form></span></p>

                </div>
                <Footer />
            </div>
        )
    }
}
