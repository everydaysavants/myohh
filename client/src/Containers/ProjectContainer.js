import React from 'react';
import Header from './../components/Header'
import Footer from './../components/Footer'
import { Panel, Row, PanelGroup, Badge } from 'react-bootstrap';
import { Link } from 'react-router';
import axios from 'axios';
import LessonBoxes from '../components/LessonBoxes';

export default class ProjectContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            project: {
                title: "",
                description: "",
                keywords: ""
            }
        }
    }

    //TODO: This has to change. Right now I am not able to find
    //proper way to pull just one project so we will pull all three here
    //wasteful

    componentDidMount() {
        axios.get('/projects').then((projectResults) => {
            const { projectId } = this.props.params.projectId;
            if (projectResults) {
                let project = projectResults.data.filter(project => project.id == this.props.params.projectId)[0];
                this.setState({
                    project: project
                });
            }
        }).catch((e) => {
            console.log("error occurred", e);
        });
    }

    render() {
        let { id } = this.props.params;
        const imageStyle = {
            paddingBottom: '20px',
            maxWidth: '100%',
            maxHeight: '100%'
        };

        const { keywords } = this.state.project;
        let keywordBody = "";
        if (keywords) {
            keywordBody = keywords.map((keyword) => {
                return (<div key={keyword}> - {keyword}</div>);
            });
        }

        let coursePlanBody = [];
        let { coursePlan } = this.state.project;
        if (coursePlan) {
            for (let i = 0; i < coursePlan.length; i++) {
                let coursePlanItems = coursePlan[i].map((course) => {
                    return (<div key={course}> - {course}</div>);
                });
                let currentHeader = "Week " + (i + 1);
                coursePlanBody.push(<Panel key={i} header={currentHeader} eventKey={i+1}>{coursePlanItems}</Panel>)
            }
        }

        let largeText = {
            fontSize: 20,
            color: "white",
            backgroundColor: "green"
        };

        let strikeThrough = {
            fontSize: 20,
            textDecoration: 'line-through'
        };

        let bigFont = {
            fontSize: 20
        };

        return (
            <div>
                <Header selectedPage="confirm"></Header>
                <div className="container">
                    <img style={imageStyle} src="/images/carousel_1.jpg"/>
                    <h1>
                        {this.state.project.title}<Badge>{this.state.project.age} </Badge>
                    </h1>
                    <p><span style={largeText}> Try our first class for free.
                    </span></p>
                    <p><span style={strikeThrough}>Price: $150</span></p>
                    <p><span style={bigFont}>For limited time: $135 per month (4 weeks)</span></p>
                    <Panel>
                        <Panel>
                            {this.state.project.description}
                            <br/>
                        </Panel>
                        <Panel> {keywordBody} </Panel>
                        { (coursePlan) ? <PanelGroup>
                            {coursePlanBody}
                        </PanelGroup> : null }
                        <Link className="btn btn-lg btn-success" to="/app/register">Register Now</Link>
                    </Panel>
                    <p style={bigFont}>Location:</p>
                    <p>Kumon Center <br/> 8262 Highland Pointe Drive, <br/> West Chester, Ohio 45069</p>
                    {(this.state.project.classes) ?
                        <Panel header="Student will learn the following as part of this course">
                            <LessonBoxes classes={this.state.project.classes}/>
                        </Panel> : null}
                </div>
                <Footer />
            </div>
        )
    }
}