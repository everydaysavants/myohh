import React from 'react';
import Header from './../components/Header'
import Footer from './../components/Footer'
import { Panel, Row, Col } from 'react-bootstrap';
import axios from 'axios';
import UserGridRow from './../components/UserGridRow';

export default class StudentsList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            studentsList:[]
        }
    }

    handleValueEntered(e) {
        if(e.key === 'Enter') {
            let url = '/getAllSignedUpUsers?id=' + e.target.value;
            axios.get(url).then((studentsList) => {
                this.setState({
                    studentsList: studentsList.data
                })
            }).catch((error) => {

                console.log("error occurred", error);
            });
        }
    }

    render() {
        let students = this.state.studentsList.map((student, index) => {
            return (
                <UserGridRow key={student.email} name={student.name} age={student.age} email={student.email}
                             courseName={student.courseName} startDate={student.startDate} />
            );
        });

        return (
            <div>
                <Header selectedPage="studentslist"></Header>
                <div className="container">
                    <Panel header="Users list registered" bsStyle="success">
                        <input type="text" onKeyPress={this.handleValueEntered.bind(this)}></input>
                        <table className="table">
                            <thead>
                            <tr>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Age
                                </th>
                                <th>
                                    Email
                                </th>
                                <th>
                                    Course Name
                                </th>
                                <th>
                                    Start Date
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            {students}
                            </tbody>
                        </table>
                    </Panel>
                </div>
                <Footer />
            </div>
        )
    }
}