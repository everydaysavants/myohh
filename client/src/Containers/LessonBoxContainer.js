import React from 'react';
import Header from './../components/Header';
import {Link} from 'react-router';
import Footer from './../components/Footer';
import LessonBoxes from './../components/LessonBoxes';
import { Panel } from 'react-bootstrap';
import ProjectBoxes from '../components/ProjectBoxes'


export default class LessonBoxContainer extends React.Component {

    render() {

        const imageStyle = {
            paddingBottom:'20px',
            maxWidth:'100%',
            maxHeight:'100%'
        };

        return (
        <div>
            <Header selectedPage="home"></Header>
            <div className="container">
                <img style={imageStyle} src="/images/carousel_2.jpg"/>
                <Panel header="Courses" bsStyle="info">
                    <ProjectBoxes />
                </Panel>
                <Panel header="Classes" bsStyle="info">
                    <LessonBoxes />
                </Panel>
            </div>
            <Footer/>
        </div>
        )
    }
}
