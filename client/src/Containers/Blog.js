import React from 'react';
import Header from './../components/Header'
import Footer from './../components/Footer'

export default class Blog extends React.Component {
    render() {
        const imageStyle = {
            paddingBottom:'20px',
            maxWidth: '100%',
            maxHeight:'100%'
        };

        return (
            <div>
                    <Header selectedPage="contact-us"></Header>
                <div className="container">
                    <img style={imageStyle} src="/images/carousel_3.jpg"/>
                    <h1>
                        Hello World!!
                    </h1>
                    <h4>Wednesday, June 1, 2016</h4>
                    <p>
                        After a year of hard work from our team, we are opening our regular classes in West Chester location starting
                        June 10.
                    </p>
                    <p>
                        This is a milestone in our journey to provide better education and inspiration to
                        our kids. Previously we were working on afterschool setup. Now we are
                        giving more freedom to parents on choosing time and courses which best fit their schedule.
                    </p>
                    <p>
                        We started My Orange Hard Hat with strong focus on teaching technology to kids and better equip them
                        to solve tomorrow's problems. We will continue to add our courses to incorporate more tools and
                        themes to help our students to become problem solvers of tomorrow.
                    </p>
                    <p>
                        Please feel free to email us with your valuable feedback at <a href="mailto:mail@myorangehardhat.com">mail@myorangehardhat.com</a>
                    </p>
                    <h1>
                        Our First Class
                    </h1>
                    <h4>Sunday, June 12, 2016</h4>
                    <p>
                        As you all know we did our first class in June 10th. The experience was great. We did three different
                        class streams. Arduino, SnapCircuits and Scratch. We felt both students and parents enjoyed the classes.
                    </p>
                    <p>
                        We would like to thank all friends, families and parents who trusted us and participated with us in this
                        journey from the beginning.
                    </p>
                    <h1>
                        Payment page added
                    </h1>
                    <h4>Sunday, June 12, 2016</h4>
                    <p>
                    </p>
                    <p>
                        Today we are excited to inform you that we have added a new payment page. Parents can now pay
                        online from the comfort of their home. We have worked with Authorize.Net to provide safe and easy
                        payment option to parents.
                    </p>

                </div>
                <Footer />
            </div>
        )
    }
}