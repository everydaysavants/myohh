import React from 'react';
import Header from './../components/Header';
import TextRotator from './../components/TextRotator';
import { Link } from 'react-router';
import ProjectBoxes from './../components/ProjectBoxes';
import axios from 'axios';
import Footer from './../components/Footer';
import { Carousel, Button } from 'react-bootstrap'

export default class Home extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const imgUrl = '/images/Lab1thumbnailLarge.jpg';

        const carouselCaption = {
            backgroundColor: "rgba(192,192,192,0.5)"
        };

        return (
            <div className="homeContainer">
                <Header selectedPage="home"></Header>
                <Carousel>
                    <Carousel.Item>
                        <img width={1500} height={300} alt="1500x300" src="/images/carousel_1.jpg"/>
                        <Carousel.Caption>
                            <h3><span style={carouselCaption}>Learn Arduino</span></h3>
                            <p><span style={carouselCaption}>Hack one of the most popular learning computer</span></p>
                            <Link className="btn btn-lg btn-success" to="/app/register">Register Now</Link>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img width={1500} height={300} alt="1500x300" src="/images/carousel_2.jpg"/>
                        <Carousel.Caption>
                            <h3><span style={carouselCaption}>Learn Scratch</span></h3>
                            <p><span style={carouselCaption}>Inspire kids to be Makers of tomorrow</span></p>
                            <Link className="btn btn-lg btn-success" to="/app/register">Register Now</Link>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img width={1500} height={300} alt="1500x300" src="/images/carousel_3.jpg"/>
                        <Carousel.Caption>
                            <h3><span style={carouselCaption}>Learn Electronics</span></h3>
                            <p><span style={carouselCaption}>Broaden the programming understanding by improving kids' hardware skills</span></p>
                            <Link className="btn btn-lg btn-success" to="/app/register">Register Now</Link>
                        </Carousel.Caption>
                    </Carousel.Item>
                </Carousel>
                <div className="container">
                    <div className="homePageMessage" align="center">
                        <span className="lead">We teach</span>
                        <span className="rotatorStyle">
                            &nbsp; Technology &nbsp;
                        </span>
                        <span className="lead"> To inspire problem solvers of tomorrow!!</span>
                    </div>
                    <ProjectBoxes />
                </div>
                <Footer />
             </div>
        )
    }
}
