import React from 'react';
import Header from './../components/Header'
import Footer from './../components/Footer'
import { Panel } from 'react-bootstrap';

export default class ConfirmRegistration extends React.Component {

    render() {
        let { registrationId } = this.props.params;
        const title = <h2>Thank you for the registration</h2>;

        return (
            <div>
                <Header selectedPage="confirm"></Header>
                <div className="container paddingTop20">
                    <Panel header={title} bsStyle="success">
                        Your confirmation number is {registrationId}
                    </Panel>
                </div>
                <Footer />
            </div>
        )
    }
}