import React from 'react';
import ReactDom from 'react-dom';
import {Router, IndexRoute, Route, hashHistory} from 'react-router';
import Home from './containers/Home';
import Blog from './containers/Blog';
import AboutUs from './containers/AboutUs';
import LessonBoxContainer from './containers/LessonBoxContainer';
import Footer from './components/Footer';
import Login from './components/Login';
import Register from './containers/Register';
import ConfirmRegistration from './containers/ConfirmRegistration';
import StudentsList from './containers/StudentsList';
import ProjectContainer from './containers/ProjectContainer';
import Payment from './containers/Payment';

let targetElement = document.getElementById('main');

class App extends React.Component {
    render() {
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}

ReactDom.render(<Router history={hashHistory}>
    <Route path="/" component={App}>
        <IndexRoute component={Home}/>
        <Route path="/app/lessons" component={LessonBoxContainer}/>
        <Route path="/app/blog" component={Blog}/>
        <Route path="/app/about-us" component={AboutUs}/>
        <Route path="/app/login" component={Login}/>
        <Route path="/app/register" component={Register}/>
        <Route path="/app/payment" component={Payment}/>
        <Route path="/app/confirm" component={ConfirmRegistration}>
            <IndexRoute component={ConfirmRegistration}/>
            <Route path=":registrationId" component={ConfirmRegistration}/>
        </Route>
        <Route path="/app/project" component={ProjectContainer}>
            <IndexRoute component={ProjectContainer}/>
            <Route path=":projectId" component={ProjectContainer}/>
        </Route>
        <Route path="/app/studentslist" component={StudentsList}/>
    </Route>
</Router>, targetElement);
