#!/bin/bash

if [ -z $PROFILE_ENV ]; then
    PROFILE_ENV=dev
fi

java -jar -Dspring.profiles.active=$PROFILE_ENV /myohh-site.jar