FROM java:8
COPY build/libs/myohh-site*.jar /myohh-site.jar
COPY scripts/entryPoint.sh /entryPoint.sh

ENTRYPOINT ["/entryPoint.sh"]
