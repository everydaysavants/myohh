package myohh.site;

import myohh.site.model.User;
import myohh.site.repositories.UserRepository;
import org.apache.shiro.authc.*;
import org.apache.shiro.realm.Realm;

import java.util.ArrayList;

public class MyohhRealm implements Realm {
    private UserRepository userRepository;

    public MyohhRealm(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public String getName() {
        return "Myohh";
    }

    public boolean supports(AuthenticationToken token) {
        return true;
    }

    public AuthenticationInfo getAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) token;
        if(authenticate(usernamePasswordToken.getUsername(), new String(usernamePasswordToken.getPassword()))){
            return new SimpleAuthenticationInfo(token.getPrincipal(), token.getCredentials(), getName());
        }
        return null;
    }

    private boolean authenticate(String email, String password) {
        ArrayList<User> users = (ArrayList<User>) userRepository.findAll();
        return users.stream().anyMatch(user -> user.getEmail().equalsIgnoreCase(email) &&
                user.getPassword().equalsIgnoreCase(password));
    }
}
