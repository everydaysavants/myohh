package myohh.site.email;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.simpleemail.model.*;
import myohh.site.config.SiteConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class EmailSenderService {
    private SiteConfig siteConfig;
    private static final String SOURCE_EMAIL = "mail@myorangehardhat.com" ;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public EmailSenderService(SiteConfig siteConfig) {
        this.siteConfig = siteConfig;
    }

    @Async
    public void sendEmail(String toAddress, String subject, String body) {
        Destination destination = new Destination().withToAddresses(toAddress);

        Content subjectContent = new Content().withData(subject);
        Content bodyContent = new Content().withData(body);

        Body emailBody = new Body().withHtml(bodyContent);

        Message message = new Message().withBody(emailBody)
                    .withSubject(subjectContent);


        SendEmailRequest request = new SendEmailRequest(SOURCE_EMAIL, destination, message);

        try {
            AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(awsCredentials(siteConfig));
            client.setRegion(Region.getRegion(Regions.US_WEST_2));
            client.sendEmail(request);
        } catch (Exception e) {
            logger.error("Could not send email from the application");
        }

    }

    private AWSCredentials awsCredentials(SiteConfig siteConfig) {
        return new BasicAWSCredentials(siteConfig.awsAccessKeyId(), siteConfig.awsSecretKey());
    }
}
