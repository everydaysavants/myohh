package myohh.site.util;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static java.lang.String.format;

@Component
public class ClassPathFileReader {
    public final String STATIC_PATH = "static/responses";

    public String read(String fileName) throws IOException {
        InputStream resourceAsStream = this.getClass().getClassLoader()
                .getResourceAsStream(format("%s/%s", STATIC_PATH, fileName));
        return readFromStream(resourceAsStream);
    }

    private String readFromStream(InputStream inputStream) throws IOException {
        StringBuilder sb=new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        String read;

        while((read=br.readLine()) != null) {
            sb.append(read);
        }

        br.close();
        return sb.toString();
    }
}