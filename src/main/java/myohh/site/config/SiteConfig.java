package myohh.site.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class SiteConfig {
    private Environment environment;

    @Autowired
    public SiteConfig(Environment environment) {
        this.environment = environment;
    }

    public String serviceUrl(){
        return environment.getProperty("service.url");
    }
    public String awsAccessKeyId(){
        return environment.getProperty("aws.access.key.id");
    }
    public String awsSecretKey(){
        return environment.getProperty("aws.secret.key");
    }
}
