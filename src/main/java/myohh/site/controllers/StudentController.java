package myohh.site.controllers;

import myohh.site.email.EmailSenderService;
import myohh.site.model.SignUpRequest;
import myohh.site.model.User;
import myohh.site.repositories.UserRepository;
import myohh.site.util.ClassPathFileReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.xml.bind.ValidationException;
import java.io.IOException;
import java.util.List;

@RestController
public class StudentController {
    private UserRepository userRepository;

    @Autowired
    public StudentController(UserRepository userRepository) throws IOException {
        this.userRepository = userRepository;
    }

    @RequestMapping(value = "/getAllSignedUpUsers", method = RequestMethod.GET)
    public ResponseEntity<List<User>> getAllSignedUpUsers(@RequestParam("id") String guid) throws ValidationException, IOException {

        if ("43a67fe9".equals(guid)) {
            return new ResponseEntity<>((List<User>) userRepository.findAll(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
