package myohh.site.controllers;

import myohh.site.email.EmailSenderService;
import myohh.site.model.SignUpRequest;
import myohh.site.model.User;
import myohh.site.repositories.UserRepository;
import myohh.site.util.ClassPathFileReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.xml.bind.ValidationException;
import java.io.IOException;

@RestController
public class SignUpController {
    private UserRepository userRepository;
    private EmailSenderService emailSenderService;
    private ClassPathFileReader classPathFileReader;

    @Autowired
    public SignUpController(UserRepository userRepository, EmailSenderService emailSenderService,
                            ClassPathFileReader classPathFileReader) throws IOException {
        this.userRepository = userRepository;
        this.emailSenderService = emailSenderService;
        this.classPathFileReader = classPathFileReader;
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity signUp(@RequestBody @Valid SignUpRequest signUpRequest) throws ValidationException, IOException {

        String thankYouEmail = classPathFileReader.read("thankyouemail.html");
        userRepository.save(buildUser(signUpRequest));
        User user = userRepository.findByEmail(signUpRequest.getEmail());
        String userID = user.getId();
        String userName = user.getName();

        //TODO: Email formatting is not working create a new template
        thankYouEmail = thankYouEmail.replace("__REGISTRATIONNUMBER__", userID);
        thankYouEmail = thankYouEmail.replace("__FIRSTNAME__", userName);
        emailSenderService.sendEmail(signUpRequest.getEmail(), "Registration Confirmation", thankYouEmail);
        return new ResponseEntity<>(userID, HttpStatus.OK);
    }

    private User buildUser(SignUpRequest signUpRequest) {
        User user = new User();
        user.setEmail(signUpRequest.getEmail());
        user.setName(String.format("%s %s", signUpRequest.getFirstName(), signUpRequest.getLastName()));
        user.setPassword(signUpRequest.getPassword());
        user.setRegisteredEvent(signUpRequest.getEventSession());
        user.setAge(signUpRequest.getAge());
        user.setCourseName(signUpRequest.getCourseName());
        user.setStartDate(signUpRequest.getStartDate());
        return user;
    }
}
