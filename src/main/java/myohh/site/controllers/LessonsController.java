package myohh.site.controllers;

import com.amazonaws.util.json.JSONObject;
import myohh.site.model.Lesson;
import myohh.site.repositories.LessonRepository;
import myohh.site.util.ClassPathFileReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

@RestController
public class LessonsController {

    private ClassPathFileReader classPathFileReader;
    private LessonRepository lessonRepository;

    @Autowired
    public LessonsController(ClassPathFileReader classPathFileReader, LessonRepository lessonRepository) {
        this.classPathFileReader = classPathFileReader;
        this.lessonRepository = lessonRepository;
    }

    @RequestMapping(value = "/getLessons", produces = "application/json")
    public ResponseEntity<List<Lesson>> getLessons() {
        return new ResponseEntity(lessonRepository.getAllLessons(), HttpStatus.OK);
    }

    @RequestMapping(value = "/getHomePageLessons", produces = "application/json")
    public ResponseEntity<List<Lesson>> getHomePageLessons() {
        return new ResponseEntity(lessonRepository.getAllHomePageLessons(), HttpStatus.OK);
    }

    @RequestMapping(value = "/lessons", produces = "application/json")
    public ResponseEntity<String> lessons() {
        try {
            return new ResponseEntity<>(classPathFileReader.read("lessons.json"), OK);
        } catch(Exception ex) {
            return new ResponseEntity<>("Error reading file lessons.json", BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/projects", produces = "application/json")
    public ResponseEntity<String> projects() {
        try {
            return new ResponseEntity<>(classPathFileReader.read("projects.json"), OK);
        } catch(Exception ex) {
            return new ResponseEntity<>("Error reading file projects.json", BAD_REQUEST);
        }
    }
}
