package myohh.site.controllers;

import myohh.site.model.LoginRequest;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

import static com.google.common.collect.ImmutableMap.of;

@Controller
public class LoginController {
    @RequestMapping(value="/login", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> postForlogin(@RequestBody LoginRequest request) {
        UsernamePasswordToken token = new UsernamePasswordToken(request.getEmail(), request.getPassword());
        try {
            SecurityUtils.getSubject().login(token);
        } catch (AuthenticationException e) {
            return new ResponseEntity<>(of("status", false), HttpStatus.OK);
        }
        return new ResponseEntity<>(of("status", true), HttpStatus.OK);

    }
    @RequestMapping("/authStatus")
    public ResponseEntity<Boolean> authStatus() {
        return new ResponseEntity<>(SecurityUtils.getSubject().isAuthenticated(), HttpStatus.OK);
    }

}
