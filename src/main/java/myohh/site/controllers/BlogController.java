package myohh.site.controllers;

import myohh.site.email.EmailSenderService;
import myohh.site.model.SignUpRequest;
import myohh.site.model.User;
import myohh.site.repositories.UserRepository;
import myohh.site.util.ClassPathFileReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.xml.bind.ValidationException;
import java.io.IOException;
import java.util.List;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

@RestController
public class BlogController {
    private ClassPathFileReader classPathFileReader;

    @Autowired
    public BlogController(UserRepository userRepository) throws IOException {
        this.classPathFileReader = classPathFileReader;
    }

    @RequestMapping(value = "/getAllBlogs", method = RequestMethod.GET)
    public ResponseEntity<String> getAllBlogs() throws ValidationException, IOException {

        try {
            return new ResponseEntity<>(classPathFileReader.read("blogs.json"), OK);
        } catch(Exception ex) {
            return new ResponseEntity<>("Error reading file lessons.json", BAD_REQUEST);
        }
    }
}