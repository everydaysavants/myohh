package myohh.site.model.validators;

import myohh.site.model.SignUpRequest;
import myohh.site.model.User;
import myohh.site.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class UserConstraintValidator implements ConstraintValidator<ValidUser, String> {
    private UserRepository userRepository;

    @Autowired
    public UserConstraintValidator(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @Override
    public void initialize(ValidUser constraintAnnotation) {

    }

    @Override
    public boolean isValid(String email, ConstraintValidatorContext context) {
        User existingUser = userRepository.findByEmail(email);
        if(existingUser == null || existingUser.getEmail() == null || existingUser.getEmail().isEmpty())
            return true;
        return false;
    }
}
