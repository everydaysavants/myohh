package myohh.site.model.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Created by kon7593 on 4/29/16.
 */

@Documented
@Constraint(validatedBy = UserConstraintValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidUser {
    String message() default "{ ValidUser }";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
