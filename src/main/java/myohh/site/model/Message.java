package myohh.site.model;

/**
 * Created by kon7593 on 4/29/16.
 */
public class Message {
    public enum MessageType {
        SUCCESS, INFO, WARNING, ERROR
    }

    private String message;
    private MessageType type;

    public Message() {
    }

    public Message(MessageType type, String message) {
        super();
        this.message = message;
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }
}
