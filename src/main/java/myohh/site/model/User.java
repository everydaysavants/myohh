package myohh.site.model;

import org.springframework.data.annotation.Id;

public class User {
    @Id
    private String id;
    private String email;
    private String password;
    private String name;
    private String registeredEvent;
    private String age;
    private String courseName;
    private String startDate;

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegisteredEvent() {
        return registeredEvent;
    }

    public void setRegisteredEvent(String registeredEvent) {
        this.registeredEvent = registeredEvent;
    }
}
