package myohh.site.model;

import java.util.Date;
import java.util.List;

/**
 * Created by surajadhikari on 7/14/15.
 */

public class Lesson extends LessonBase{
    private String thumbnailUrl;
    private String ageGroup;
    private List<String> steps;
    private List<String> components;
    private Boolean inFrontPage;
    private Boolean offerNow;
    private List<Address> locations;
    private List<Date> dates;

    public List<Address> getLocations() {
        return locations;
    }

    public void setLocations(List<Address> locations) {
        this.locations = locations;
    }

    public List<Date> getDates() {
        return dates;
    }

    public void setDates(List<Date> dates) {
        this.dates = dates;
    }

    public Boolean getOfferNow() {
        return offerNow;
    }

    public void setOfferNow(Boolean offerNow) {
        this.offerNow = offerNow;
    }

    public String getAgeGroup() {
        return ageGroup;
    }

    public void setAgeGroup(String ageGroup) {
        this.ageGroup = ageGroup;
    }

    public Boolean getInFrontPage() {
        return inFrontPage;
    }

    public void setInFrontPage(Boolean inFrontPage) {
        this.inFrontPage = inFrontPage;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public List<String> getSteps() {
        return steps;
    }

    public void setSteps(List<String> steps) {
        this.steps = steps;
    }

    public List<String> getComponents() {
        return components;
    }

    public void setComponents(List<String> components) {
        this.components = components;
    }
}
