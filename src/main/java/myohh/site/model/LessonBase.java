package myohh.site.model;

/**
 * Created by surajadhikari on 10/1/15.
 */
//TODO: Maybe this can be base class for lesson; Not sure if extended model works as regular pojo?
public class LessonBase {

    public LessonBase() {

    }

    public LessonBase(Lesson lesson) {
        this.title = lesson.getTitle();
        this.summary = lesson.getSummary();
        this.id = lesson.getId();
    }

    private String id;
    private String title;
    private String summary;

    public String getTitle() {
        return title;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
