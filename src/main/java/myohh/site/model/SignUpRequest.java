package myohh.site.model;

import myohh.site.model.validators.ValidUser;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.util.Assert;

import javax.validation.constraints.AssertFalse;
import javax.validation.constraints.NotNull;

public class SignUpRequest {

    @NotNull(message = "email.notEmpty")
    @NotEmpty(message = "email.notEmpty")
    @ValidUser(message = "email.existingUser")
    private String email;
    private String password;
    private String firstName;
    private String lastName;

    @NotNull(message = "age.notEmpty")
    @NotEmpty(message = "age.notEmpty")
    private String age;

    @NotNull(message = "courseName.notEmpty")
    @NotEmpty(message = "courseName.notEmpty")
    private String courseName;

    @NotNull(message = "startDate.notEmpty")
    @NotEmpty(message = "startDate.notEmpty")
    private String startDate;
    private String eventSession;

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEventSession() {
        return eventSession;
    }

    public void setEventSession(String eventSession) {
        this.eventSession = eventSession;
    }
}
