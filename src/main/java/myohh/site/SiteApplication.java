package myohh.site;


import myohh.site.repositories.UserRepository;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.filter.authc.LogoutFilter;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.client.RestTemplate;

import javax.servlet.Filter;

import static java.util.Arrays.asList;

@SpringBootApplication
@EnableAsync
public class SiteApplication{
    @Autowired
    UserRepository userRepository;

    public static void main(String[] args){
        SpringApplication.run(SiteApplication.class);
    }

    @Bean
    public FilterRegistrationBean shiroFilter() throws Exception {
        ShiroFilterFactoryBean factory = new ShiroFilterFactoryBean();
        factory.setSecurityManager(securityManager());
        return new FilterRegistrationBean((Filter) factory.getObject());
    }

    @Bean
    public FilterRegistrationBean logoutFilter() {
        FilterRegistrationBean logoutFilter = new FilterRegistrationBean(new LogoutFilter());
        logoutFilter.setUrlPatterns(asList("/logout"));
        return  logoutFilter;
    }

    @Bean
    public org.apache.shiro.mgt.SecurityManager securityManager() {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(myohhRealm());
        return securityManager;
    }

    @Bean
    public Realm myohhRealm() {
        return new MyohhRealm(userRepository);
    }

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @Bean(name = "messageSource")
    public ReloadableResourceBundleMessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageBundle = new ReloadableResourceBundleMessageSource();
        messageBundle.setBasename("classpath:messages/messages");
        messageBundle.setDefaultEncoding("UTF-8");
        return messageBundle;
    }
}
