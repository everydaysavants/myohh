package myohh.site.repositories;

import myohh.site.model.Address;
import myohh.site.model.LessonBase;
import myohh.site.model.Lesson;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class LocalLessonRepository implements LessonRepository {

    @Override
    public List<Lesson> getAllLessons() {

        Lesson lesson1 = new Lesson() {
            {
                setId("MOHH101");
                setTitle("Getting Started Basic Circuits Using Snapcircuit Jr.");
                setSummary("At MOHH headquarter we love Snapcircuit. It is quick and easy way to get started in basic" +
                        "elecronics. Best part about this is: Kids love it.");
                List<String> components = new ArrayList<String>() {
                    {
                        add("Resistors");
                        add("Wires");
                        add("LEDs");
                    }
                };

                List<String> steps = new ArrayList<String>() {
                    {
                        add("Step 1");
                        add("Step 2");
                        add("Step 3");

                    }
                };

                Address address = new Address() {
                    {
                        setAddressName("Kumon Center WC");
                        setHouseNumber(8202);
                        setStreetAddress("Highland Point Drive West");
                        setCity("West Chester Township");
                        setZipCode("45011");
                    }
                };

                List<Address> locations = new ArrayList<Address>() {
                    {
                        add(address);
                    }
                };

                List<Date> dates = new ArrayList<Date>() {
                    {
                        add(new GregorianCalendar(2015, 11, 1, 13, 0).getTime());
                    }
                };

                setComponents(components);
                setDates(dates);
                setLocations(locations);
                setOfferNow(true);
                setSteps(steps);
                setAgeGroup("7 & up");
                setInFrontPage(true);
            }
        };
        Lesson lesson2 = new Lesson() {
            {
                setId("MOHH102");
                setTitle("Make My Own: Build Using Breadboard and Other Components");
                setSummary("After completing first lesson kids will be ready to actually start making on their own." +
                        "Breadboard is best tool for designing circuits and has been used by professionals and" +
                        "hobbyists alike.");
                setAgeGroup("7 & up");
                List<String> components = new ArrayList<String>() {
                    {
                        add("Resistors");
                        add("Wires");
                        add("LEDs");
                    }
                };
                setComponents(components);
                List<String> steps = new ArrayList<String>() {
                    {
                        add("Step 1: Make tentative measurement of where you want to add LED and controller(GEMMA) in your hat.");
                        add("Step 2: Make holes in those places where LEDs and GEMMA can be sewn. ");
                        add("Step 3: Program GEMMA and test it with croc");
                        add("Step 4: Sew LEDs together using steel thread");
                        add("Step 5: Add battery pack and tape it to the hat");
                        add("Step 6: Turn on the battery pack");
                    }
                };
                Address address = new Address() {
                    {
                        setAddressName("Kumon Center WC");
                        setHouseNumber(8202);
                        setStreetAddress("Highland Point Drive West");
                        setCity("West Chester Township");
                        setZipCode("45011");
                    }
                };

                List<Address> locations = new ArrayList<Address>() {
                    {
                        add(address);
                    }
                };

                List<Date> dates = new ArrayList<Date>() {
                    {
                        add(new GregorianCalendar(2015, 11, 1, 13, 0).getTime());
                    }
                };

                setDates(dates);
                setLocations(locations);
                setOfferNow(true);
                setSteps(steps);
                setInFrontPage(true);
            }
        };
        Lesson lesson3 = new Lesson() {
            {
                setId("MOHH103");
                setTitle("My Intelligent Letter Box");
                setSummary("After completing first two lessons kids will be ready to build their own " +
                        "circuit. We decided that this is the best starter project for student because it invovles" +
                        "basic parts that students have learned in earlier lessons and students will get introduced " +
                        "to sensors. This will also set stage for " +
                        "using programming devices like Arduino, Raspberry Pi etc");
                List<String> components = new ArrayList<String>() {
                    {
                        add("Resistors");
                        add("Wires");
                        add("LEDs");
                    }
                };
                setComponents(components);
                List<String> steps = new ArrayList<String>() {
                    {
                        add("Step 1: Make tentative measurement of where you want to add LED and controller(GEMMA) in your hat.");
                        add("Step 2: Make holes in those places where LEDs and GEMMA can be sewn. ");
                        add("Step 3: Program GEMMA and test it with croc");
                        add("Step 4: Sew LEDs together using steel thread");
                        add("Step 5: Add battery pack and tape it to the hat");
                        add("Step 6: Turn on the battery pack");
                    }
                };

                Address address = new Address() {
                    {
                        setAddressName("Kumon Center WC");
                        setHouseNumber(8202);
                        setStreetAddress("Highland Point Drive West");
                        setCity("West Chester Township");
                        setZipCode("45011");
                    }
                };

                List<Address> locations = new ArrayList<Address>() {
                    {
                        add(address);
                    }
                };

                List<Date> dates = new ArrayList<Date>() {
                    {
                        add(new GregorianCalendar(2015, 11, 1, 13, 0).getTime());
                    }
                };

                setDates(dates);
                setLocations(locations);
                setOfferNow(true);
                setSteps(steps);
                setAgeGroup("7 & up");
                setInFrontPage(true);
            }
        };
        Lesson lesson4 = new Lesson() {
            {
                setId("MOHH104");
                setTitle("Make My LetterBox Smart: Introduction To Arduino");
                setSummary("This is the first project in programming. Students will continue to expand" +
                        "their knowledge in electronics to jump in to programming world.");
                List<String> components = new ArrayList<String>() {
                    {
                        add("Resistors");
                        add("Wires");
                        add("LEDs");
                    }
                };
                setComponents(components);
                List<String> steps = new ArrayList<String>() {
                    {
                        add("Step 1: Make tentative measurement of where you want to add LED and controller(GEMMA) in your hat.");
                        add("Step 2: Make holes in those places where LEDs and GEMMA can be sewn. ");
                        add("Step 3: Program GEMMA and test it with croc");
                        add("Step 4: Sew LEDs together using steel thread");
                        add("Step 5: Add battery pack and tape it to the hat");
                        add("Step 6: Turn on the battery pack");
                    }
                };

                Address address = new Address() {
                    {
                        setAddressName("Kumon Center WC");
                        setHouseNumber(8202);
                        setStreetAddress("Highland Point Drive West");
                        setCity("West Chester Township");
                        setZipCode("45011");
                    }
                };

                List<Address> locations = new ArrayList<Address>() {
                    {
                        add(address);
                    }
                };

                List<Date> dates = new ArrayList<Date>() {
                    {
                        add(new GregorianCalendar(2015, 11, 1, 13, 0).getTime());
                    }
                };

                setDates(dates);
                setLocations(locations);
                setOfferNow(false);
                setSteps(steps);
                setAgeGroup("8 & up");
                setInFrontPage(true);
            }
        };


        List<Lesson> lessons = new ArrayList<Lesson>() {
            {
                add(lesson1);
                add(lesson2);
                add(lesson3);
                add(lesson4);
            }
        };
        return lessons;
    }

    @Override
    public Lesson getLesson(String id) {
        List<Lesson> lessons = getAllLessons();
        Optional<Lesson> lesson = lessons.stream().filter(p -> p.getId().equals(id)).findFirst();
        return lesson.isPresent() ? lesson.get() : null;
    }

    @Override
    public Map<String, List<String>> getAllHomePageLessonTitles() {
        List<Lesson> lessons = getAllLessons();
        //TODO: Make it work when List is empty
        List<String> frontPageLessonTitles = lessons.stream()
                .filter(p -> p.getInFrontPage())
                .map(lesson -> lesson.getTitle())
                .collect(Collectors.toList());
        Map<String, List<String>> titlesMap = new HashMap<>();
        titlesMap.put("titles", frontPageLessonTitles);
        return titlesMap;
    }

    @Override
    public Map<String, List<String>> getAllHomePageLessonSummaries() {
        List<Lesson> lessons = getAllLessons();
        //TODO: Make it work when List is empty
        List<String> frontPageLessonSummaries = lessons.stream()
                .filter(p -> p.getInFrontPage().equals(true))
                .map(lesson -> lesson.getSummary())
                .collect(Collectors.toList());
        Map<String, List<String>> summaryMap = new HashMap<>();
        summaryMap.put("summaries", frontPageLessonSummaries);
        return summaryMap;
    }

    @Override
    public Map<String, List<String>> getAllFrontPageLessonAgeGroups() {
        List<Lesson> lessons = getAllLessons();
        //TODO: Make it work when list is empty
        List<String> frontPageLessonAgeGroups = lessons.stream()
                .filter(p -> p.getInFrontPage().equals(true))
                .map(lesson -> lesson.getAgeGroup())
                .collect(Collectors.toList());
        Map<String, List<String>> ageMap = new HashMap<>();
        ageMap.put("ageGroup", frontPageLessonAgeGroups);
        return ageMap;
    }

    @Override
    public List<LessonBase> getAllHomePageLessons() {
        //TODO: Make it work when list is empty
        List<LessonBase> lessonBases = getAllLessons().stream()
                .filter(p -> p.getInFrontPage().equals(true))
                .map(lesson -> new LessonBase(lesson))
                .collect(Collectors.toList());
        return lessonBases;

    }
}
