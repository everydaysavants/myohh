package myohh.site.repositories;

import myohh.site.model.LessonBase;
import myohh.site.model.Lesson;

import java.util.List;
import java.util.Map;

/**
 * Created by surajadhikari on 7/14/15.
 */
public interface LessonRepository {

    List<Lesson> getAllLessons();

    Lesson getLesson(String id);

    List<LessonBase> getAllHomePageLessons();

    Map<String,List<String>> getAllHomePageLessonTitles();

    Map<String,List<String>> getAllFrontPageLessonAgeGroups();

    Map<String,List<String>> getAllHomePageLessonSummaries();


}
